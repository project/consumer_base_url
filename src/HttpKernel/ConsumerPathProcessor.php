<?php

namespace Drupal\consumer_base_url\HttpKernel;

use Drupal\Component\Utility\UrlHelper;
use Drupal\consumer_base_url\BaseUrlProvider;
use Drupal\consumers\Negotiator;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the outbound path for consumers.
 *
 * This outbound path process will generate correct URLs in backend for viewing
 * of published/unpublished articles in frontend. That's important for editorial
 * workflow.
 */
class ConsumerPathProcessor implements OutboundPathProcessorInterface {

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The base url provider service.
   *
   * @var \Drupal\consumer_base_url\BaseUrlProvider
   */
  protected $baseUrlProvider;

  /**
   * The consumers negotiator service.
   *
   * @var \Drupal\consumers\Negotiator
   */
  protected $consumerNegotiator;

  /**
   * Constructs a ConsumerPathProcessor object.
   *
   * @param \Drupal\consumers\Negotiator $consumer_negotiator
   *   The consumer negotiator.
   * @param \Drupal\consumer_base_url\BaseUrlProvider $base_url_provider
   *   The base url provider service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The path alias manager.
   */
  public function __construct(Negotiator $consumer_negotiator, BaseUrlProvider $base_url_provider, AliasManagerInterface $alias_manager) {
    $this->consumerNegotiator = $consumer_negotiator;
    $this->baseUrlProvider = $base_url_provider;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (($consumer = $this->consumerNegotiator->negotiateFromRequest($request))
      && $consumer_base_url = $this->baseUrlProvider->loadBaseUrl($consumer)) {

      // Only act on valid internal paths and when a domain loads.
      if (empty($path) || !empty($options['external'])) {
        return $path;
      }

      // Do not change Base URL in GraphQL 4.x and 3.x requests.
      if (consumer_base_url_is_graphql_request($request)) {
        return $path;
      }

      // Workaround for /admin paths without admin route handling.
      if ($path === '/admin' || strpos($path, '/admin/') !== FALSE) {
        return $path;
      }

      // @todo Check how to get configured sites default langcode here.
      $langcode = NULL;
      if (!empty($options['language'])) {
        $langcode = $options['language']->getId();
      }

      $alias = $this->aliasManager->getPathByAlias($path, $langcode);
      $url = Url::fromUserInput($alias, $options);
      if ($this->baseUrlProvider->isConsumerRoute($url)) {
        $options['base_url'] = trim($consumer_base_url, '/');
        $options['absolute'] = TRUE;
        return UrlHelper::encodePath($path);
      }
    }
    return $path;
  }

}
