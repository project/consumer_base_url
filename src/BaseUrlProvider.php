<?php

namespace Drupal\consumer_base_url;

use Drupal\consumers\Entity\Consumer;
use Drupal\Core\Url;

/**
 * Consumers base URL provider.
 *
 * @package Drupal\consumer_base_url
 */
class BaseUrlProvider {

  /**
   * Array of routes handled by consumer.
   *
   * @var string[]
   *
   * @todo Make consumer routes alterable and/or configurable.
   */
  protected $consumerRoutes = [
    '<front>',
    'entity.media.canonical',
    'entity.node.canonical',
    'entity.node.latest_version',
    'entity.node.revision',
    'entity.taxonomy_term.canonical',
    'entity.user.canonical',
  ];

  /**
   * Load the base URL for a given consumer.
   *
   * @param \Drupal\consumers\Entity\Consumer $consumer
   *   Consumer entity to load base URL for.
   *
   * @return string|null
   *   Return base URL of consumer as string or NULL.
   */
  public function loadBaseUrl(Consumer $consumer) {
    if (!$consumer->get('base_url')->isEmpty()) {
      return $consumer->get('base_url')->value;
    }
    return NULL;
  }

  /**
   * Check if URL is a consumer route.
   *
   * @param \Drupal\Core\Url $url
   *   The URL object of processed path.
   *
   * @return bool
   *   Return TRUE if URL is a consumer route.
   */
  public function isConsumerRoute(Url $url) {
    if ($url->isRouted()
      && in_array($url->getRouteName(), $this->consumerRoutes)) {

      return TRUE;
    }
    return FALSE;
  }

}
