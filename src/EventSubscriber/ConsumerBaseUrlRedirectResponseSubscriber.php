<?php

namespace Drupal\consumer_base_url\EventSubscriber;

use Drupal\Component\HttpFoundation\SecuredRedirectResponse;
use Drupal\consumer_base_url\BaseUrlProvider;
use Drupal\consumers\Negotiator;
use Drupal\Core\EventSubscriber\RedirectResponseSubscriber;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Utility\UnroutedUrlAssemblerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Override RedirectResponseSubscriber of Drupal Core for decoupled projects.
 *
 * @package Drupal\consumer_base_url\EventSubscriber
 */
class ConsumerBaseUrlRedirectResponseSubscriber extends RedirectResponseSubscriber implements EventSubscriberInterface {

  /**
   * The base url provider service.
   *
   * @var \Drupal\consumer_base_url\BaseUrlProvider
   */
  protected $baseUrlProvider;

  /**
   * The consumers negotiator service.
   *
   * @var \Drupal\consumers\Negotiator
   */
  protected $consumerNegotiator;

  /**
   * Constructs a RedirectResponseSubscriber object.
   *
   * @param \Drupal\Core\Utility\UnroutedUrlAssemblerInterface $url_assembler
   *   The unrouted URL assembler service.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   * @param \Drupal\consumers\Negotiator $consumers_negotiator
   *   The consumer negotiator.
   * @param \Drupal\consumer_base_url\BaseUrlProvider $base_url_provider
   *   The base url provider service.
   */
  public function __construct(UnroutedUrlAssemblerInterface $url_assembler, RequestContext $request_context, Negotiator $consumers_negotiator, BaseUrlProvider $base_url_provider) {
    parent::__construct($url_assembler, $request_context);
    $this->consumerNegotiator = $consumers_negotiator;
    $this->baseUrlProvider = $base_url_provider;
  }

  /**
   * Allows manipulation of the response object when performing a redirect.
   *
   * Overridden checkRedirectUrl() also checks configured frontend domain
   * as a TrustedRedirectResponse.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The Event to process.
   *
   * @throws \Drupal\consumers\MissingConsumer
   */
  public function checkRedirectUrl(ResponseEvent $event) {
    $response = $event->getResponse();
    if ($response instanceof RedirectResponse) {
      $request = $event->getRequest();

      // Let the 'destination' query parameter override the redirect target.
      // If $response is already a SecuredRedirectResponse, it might reject the
      // new target as invalid, in which case proceed with the old target.
      $destination = $request->query->get('destination');
      if ($destination) {
        // The 'Location' HTTP header must always be absolute.
        $destination = $this->getDestinationAsAbsoluteUrl($destination, $request->getSchemeAndHttpHost());
        try {
          $response->setTargetUrl($destination);
        }
        catch (\InvalidArgumentException $e) {
        }
      }

      // Regardless of whether the target is the original one or the overridden
      // destination, ensure that all redirects are safe.
      if (!($response instanceof SecuredRedirectResponse)) {
        try {
          $consumer = $this->consumerNegotiator->negotiateFromRequest($request);
          $host = parse_url($response->getTargetUrl(), PHP_URL_HOST);

          if (!empty($host)
            && is_string($host)
            && ($consumer_base_url = $this->baseUrlProvider->loadBaseUrl($consumer))
            && strpos($consumer_base_url, $host) !== FALSE) {

            $safe_response = TrustedRedirectResponse::createFromRedirectResponse($response);
          }
          else {
            // SecuredRedirectResponse is an abstract class that requires a
            // concrete implementation. Default to LocalRedirectResponse, which
            // considers only redirects to within the same site as safe.
            $safe_response = LocalRedirectResponse::createFromRedirectResponse($response);
          }
          $safe_response->setRequestContext($this->requestContext);
        }
        catch (\InvalidArgumentException $e) {
          // If the above failed, it's because the redirect target wasn't
          // local. Do not follow that redirect. Display an error message
          // instead. We're already catching one exception, so trigger_error()
          // rather than throw another one.
          // We don't throw an exception, because this is a client error
          // rather than a server error.
          $message = 'Redirects to external URLs are not allowed by default, use \Drupal\Core\Routing\TrustedRedirectResponse for it.';
          $safe_response = new Response($message, 400);
          trigger_error($message, E_USER_ERROR);
        }
        $event->setResponse($safe_response);
      }
    }
  }

}
