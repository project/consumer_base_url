# Consumer Base URL

The Consumer Base URL module provides base URL per consumer and use it for
entity canonical URLs, URL tokens and other stuff.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/consumer_base_url).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/consumer_base_url).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Consumers](https://www.drupal.org/project/consumers)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Manage > Configuration > Web-Services > Consumers.
2. Edit consumer you want to add a base URL for.
3. Add base URL in following format https://example.com.
4. Save consumer.
5. Maybe you have to clear caches if you add/edit a base URL for an existing consumer.


## Maintainers

- Steffen Schlaer - [IT-Cru](https://www.drupal.org/u/it-cru)
